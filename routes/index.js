var express = require('express');
var router = express.Router();
//middleware
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var controller = require('../app/controller');

/* GET home page. */

router.get('/', controller.index);
router.get('/new', controller.new);
router.post('/create', multipartMiddleware, controller.create);
router.post('/destroy', controller.destroy);
router.post('/edit', controller.edit);
router.post('/update', controller.update);
router.get('/:id', controller.find);
router.get('/tag/:tag', controller.loadTagged);

module.exports = router;
