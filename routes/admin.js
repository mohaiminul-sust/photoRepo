var express = require('express');
var router = express.Router();

/* GET users listing. */
var controller = require('../app/controller');

router.get('/', controller.admin.index);


module.exports = router;
