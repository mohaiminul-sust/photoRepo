var express = require('express');
var app = express();
var path = require('path');
var favicon = require('serve-favicon');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');

//load route partials
var mainroutes = require('./routes/index');
var adminroutes = require('./routes/admin');
var debugroutes = require('./routes/debug');

//Setup View Engines
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//config mongodb url
mongoose.connect('mongodb://dbadmin_tan:tanpura@ds061506.mlab.com:61506/photorepo');

//basic configs
app.use(logger('dev'));
// app.use(methodOverride('_method'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
// app.use(express.favicon(__dirname + '/public/images/favicon.ico'));

//site
app.use('/', mainroutes);
app.use('/admin', adminroutes);
app.use('/debug', debugroutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
