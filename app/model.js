var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//schema
var postSchema = new Schema({
    title: String,
    description: String,
    image: String,
    image_id: String,
    created_at: Date
});

//model
var Post = mongoose.model('Post', postSchema);

//export model
module.exports = Post;
