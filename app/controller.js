
var cloudinary = require('cloudinary');
var Model = require('./model');

//Cloudinary Settings
cloudinary.config({
    cloud_name: 'infancyit',
    api_key: '244969369746786',
    api_secret: 'reoXjSt5C34Bw6lXqs3d1s-sXWI'
});

module.exports = {
  index: function (req, res) {
      Model.find({}, function (err, posts) {
          if(err) res.send(err);
          // cloudinary.api.tags(function(result){
          //     res.render('pages/index', {tags: result.resources, posts: posts})
          // });
          res.render('pages/index', {posts: posts});
      });
  },
  loadTagged: function (req, res) {
      var tag = req.params.tag;

      Model.find({}, function (err, posts) {
          if(err) res.send(err);
          cloudinary.api.resources_by_tag(tag, function(result){
              res.render('pages/index', {posts: result.resources})
          });
      });
  },
  new: function (req, res) {
      res.render('pages/new');
  },
  create: function (req, res) {
      cloudinary.v2.uploader.upload(req.files.image.path,
        { tags: req.body.tags },
        { width: 1000, height: 500, crop: "limit" },
        function(err, result) {
          // Creating post model
          var post = new Model({
              title: req.body.title,
              description: req.body.description,
              created_at: new Date(),
              image: req.body.image,
              image_id: result.public_id
          });
          //saving
          post.save(function (err) {
              if(err){
                  res.send(err)
              }
              // Redirect to base
              res.redirect('/');
          });
        });
  },
  find: function (req, res) {
      var id = req.params.id;
      Model.find({image_id: id}, function (err, post) {
          if (err) res.send(err);
          res.render('pages/single', {post: post, image: cloudinary.image, image_url: cloudinary.url});
      })
  },
  destroy: function (req, res) {
      var imageId = req.body.image_id;
      cloudinary.v2.uploader.destroy(imageId, function (error, result) {
              Model.findOneAndRemove({ image_id: imageId }, function(err) {
                  if (err) res.send(err);
                  res.redirect('/');
              });
      });
  },
  edit: function (req, res) {
      Model.find({image_id: req.params.id}, function (err, posts) {
          if(err) res.send(err);
          res.render('pages/edit', {post: posts[0]});
      });
  },
  update: function (req, res) {
      var oldName = req.body.old_id;
      var newName = req.body.image_id;
      cloudinary.v2.uploader.rename(oldName, newName,
          function(error, result) {
              if (error) res.send(error);
              Model.findOneAndUpdate({image_id: oldName},
                  Object.assign({}, req.body, {image: result.url}), function (err) {
                    if (err) res.send(err);
                    res.redirect('/');
              })
          }
      );
  },
  admin: {
        index: function (req, res) {
            var q = req.query.q;
            var callback = function(result){
                // populate search
                var searchValue = '';
                if(q){
                    searchValue = q;
                }
                res.render('admin/index', {posts: result.resources, searchValue: searchValue});
            };

            // q Filter
            if(q){
                cloudinary.api.resources(callback,
                    { type: 'upload', prefix: q }
                );
            } else {
                // no q, then all
                cloudinary.api.resources(callback);
            }
        }
    }

};
